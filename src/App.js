import React from "react";

export function App() {
    return (
        <>
            <div className="hello">
                <h1>Hello, world</h1>
                <h2>Welcome, to my page!</h2>
            </div>
            <div id="date">
                <h1>Date Today : {new Date().toString()}</h1>
            </div>
        </>
    );
}
